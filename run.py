from net import Identity,Net
from greedySearch import greedySearch
from utils import Search
from flask import Flask, render_template,redirect, request, json, session
from werkzeug.utils import secure_filename

import os

app = Flask(__name__)
UPLOAD_FOLDER = 'image'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

app.secret_key = "Hello#123"

@app.route('/',methods = ["GET","POST"])
def login():
    if request.method == "POST":
        session.permanent = True
        user = request.form["userid"]
        session["user"] = int(user)
        return render_template("upload.html")
    else:
        if "user" in session:
            return render_template("upload.html")

        return render_template("login.html")   

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      f = request.files['file']
      filename = secure_filename(f.filename)
      f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
      print("upload success")
      if "user" in session:
         result = Search(os.path.join(app.config['UPLOAD_FOLDER'], filename))
         result = result[session["user"]%2]
      else:
         result = greedySearch(os.path.join(app.config['UPLOAD_FOLDER'], filename))
      return render_template('result.html',Result = result)
   return render_template("upload.html")
   
@app.route('/copied',methods = ['POST'])
def copied_text():
   if request.method == 'POST':
      copiedText = request.form['copiedText']
      print('Received : '+copiedText)
      # session.pop('userid',None)
      return json.dumps({'status':'OK'})

@app.route("/logout")
def logout():
    session.pop("user", None)
    return render_template("login.html")

if __name__ == '__main__':
   app.run(debug = True)
