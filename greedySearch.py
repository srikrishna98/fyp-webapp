import pickle
import numpy as np
import torch
import math
import torch.nn as nn
from torch import optim
import torch.nn.functional as F
from torchvision import datasets, models, transforms, utils
import glob
import os
import pickle
from torch.utils.data import Dataset, DataLoader
from PIL import Image as image
from net import Net,Identity

from utils import pad_sequence
from utils import wordixmaps


net = pickle.load(open('./pickles/model.pkl','rb'))
max_length = pickle.load(open('./pickles/max_length.pkl','rb'))

def greedySearch(photo):
    ixtoword,wordtoix = wordixmaps()

    in_text = 'startseq'
    for i in range(max_length):
        sequence = [int(wordtoix[w]) for w in in_text.split() if w in wordtoix]
        sequence = pad_sequence(sequence, maxlen=max_length)
        sequence = np.asarray(sequence)
        sequence = torch.from_numpy(sequence)
        sequence = sequence.unsqueeze(0)
        sequence = (sequence).to(torch.int64)
#         sequence = sequence.to("cuda")
        yhat = net([photo,sequence])
        #yhat = F.softmax(yhat,1)
        #print(yhat)
        yhat = yhat.cpu().data.numpy()
        #print(yhat)
        yhat = np.argmax(yhat)
        word = ixtoword[yhat]
        #print(word)
        in_text += ' ' + word
        if word == 'endseq':
            break
    final = in_text.split()
    final = final[1:-1]
    final = ' '.join(final)
    return final
