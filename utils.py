import pickle
from PIL import Image
import os
from ImageUtils import XORCompare

IMAGE_DIRECTORY = "Web-app/model_images"

def wordixmaps():
    vocab = pickle.load(open('./pickles/vocab.pkl','rb'))
    ixtoword = {}
    wordtoix = {}
    ix = 1
    for w in vocab:
        wordtoix[w] = ix
        ixtoword[ix] = w
        ix += 1
    # vocab_size = len(ixtoword) + 1 
    return (ixtoword,wordtoix)


def pad_sequence(sequence,maxlen):
        pad = maxlen - len(sequence)
        result = [0 for i in range(pad)]
        for i in sequence:
            result.append(i)
        return result

# golmal work

# Iterates through all the images in the model_images directory and compares the input image with it
# if equal image is found return file name
# else return blank
def imgIterator(userImage):
    for model_image_sample in os.listdir(IMAGE_DIRECTORY):
        sampleImage = Image.open(os.path.join(IMAGE_DIRECTORY, model_image_sample))
        temp = XORCompare(sampleImage,userImage,mode="x")
        if temp == True:
            return model_image_sample
    return ""

# See pklUtil.py for captions
# opens the pickled object and retrieves values back 
def Search(imagePath):
    im = Image.open(imagePath)
    imgEnumVal = imgIterator(im)
    model = open('pickles/model_captions.pkl', 'rb')
    captionsGenerator = pickle.load(model)
    print(captionsGenerator[imgEnumVal])
    return captionsGenerator[imgEnumVal]