from matplotlib import pyplot
import numpy as np
import torch
import math
import torch.nn as nn
from torch import optim
import torch.nn.functional as F
import string
from torchvision import datasets, models, transforms, utils
import glob
import os
import pickle
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
from PIL import Image as image
import time
import pickle


class Identity(nn.Module):
    def __init__(self):
        super(Identity, self).__init__()
        
    def forward(self, x):
        return x

vocab_size = len(pickle.load(open('./pickles/vocab.pkl','rb')))+1
embedding_dim = 200

class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        # 1 input image channel, 6 output channels, 3x3 square convolution
        # kernel
        self.incept = models.inception_v3(pretrained=True)
        self.incept.fc = Identity()
#         self.incept.to('cuda')
        # self.conv2 = nn.Conv2d(6, 16, 3)
        # an affine operation: y = Wx + b
        for param in self.incept.parameters():
            param.requires_grad = False
        for param in self.incept.AuxLogits.parameters():
            param.requires_grad = False
        self.d1 = nn.Dropout()
        self.fc1 = nn.Linear(2048,256) 
        self.emb = nn.Embedding(vocab_size,embedding_dim)
        self.d2 = nn.Dropout()
        self.lst = nn.LSTM(200,256,batch_first=True)
        self.fc2 = nn.Linear(256,256)
        self.fc3 = nn.Linear(256,vocab_size)
        self.data_transforms = transforms.Compose ([
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], 
                        [0.229, 0.224, 0.225])
        ])
        
    def forward(self, inputs):
        # Max pooling over a (2, 2) window
        photo = inputs[0]
        in_seq = inputs[1]
        photo = open(photo,"rb")
        photo = image.open(photo)
        photo = photo.convert('RGB')
        photo = photo.resize((299,299))
        photo = self.data_transforms(photo)
        photo_input = torch.zeros((1,) + photo.size())
        photo_input[0] = photo
        photo = photo_input
        # photo = photo.unsqueeze(0)
#         photo = photo.to("cuda")
        enc = self.incept(photo)
        enc = enc.reshape(enc.shape[1])
        x = self.d1(enc)
        x = self.fc1(x)
        x = F.relu(x)
        in_seq = self.emb(in_seq)
        in_seq = self.d2(in_seq)
#         in_seq = in_seq.type(torch.cuda.FloatTensor)
        in_seq = self.lst(in_seq)[0]
        in_seq = in_seq[:,-1,:]
        dec = in_seq.add(x)
        dec = F.relu(self.fc2(dec))
        output = self.fc3(dec)
        return output

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features


# net = Net()
# if torch.cuda.is_available():
#     net.to('cuda')
# print(net)