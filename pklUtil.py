import pickle

# {"name":[caption1,caption2]}
# name should be same as the name of the file in the model_images
obj = {
    "beach.jpg":["people are standing on the beach near the water","cloudy sunset at a beach"],
    "dogs.jpg":["two dogs are running through the grass","a piece of wood is carried by two dogs"],
}

model_captions = open('pickles/model_captions.pkl','wb')
pickle.dump(obj,model_captions)
model_captions.close()

# code to test if pickling works
# dbfile = open('pickles/model_captions.pkl', 'rb')      
# db = pickle.load(dbfile)
# print(db)